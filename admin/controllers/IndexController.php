<?php

namespace app\modules\admin\controllers;

class IndexController extends MainController
{
    public function actionIndex()
    {
        return $this->render('index', []);
    }
}
