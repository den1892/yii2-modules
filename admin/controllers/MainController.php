<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;

class MainController extends Controller
{
    public function init()
    {
        parent::init();
        Yii::$app->errorHandler->errorAction = 'index/error';
    }

    public function beforeAction($event)
    {
        //   die('45t');

        return parent::beforeAction($event);
    }

    private function accessCheck($redirect = null)
    {
        if (Yii::$app->user->isGuest) {
            return Yii::$app->response->redirect(Url::to($redirect ? $redirect : ['admin/login']));
        }

        return true;
    }

    public function goBack($defaultUrl = null)
    {
        return Yii::$app->getResponse()->redirect(isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '/');
    }
}
